# Assignment 2 - Agile Software Practice.

Name: Qianwen Zhang

## Client UI.

![][Home]

>>Users should login first, otherwise they can do nothing on the website(redirect)

![][Login]

>>3 ways are provide for users, including facebook, google account, and normal registration.

![][SignInWithFaceBook]

>>The user name and user profile will be shown on the page after logging in.

![][SetTimer]

>>Choose plant, minutes and write some description to start timer.

![][TimerGoes]

>>A timer will be shown on the page.


![][QuitFocusing]

>>Click Stop button, there will be an alert, you can choose whether to go on or not.


![][IfYouQuit]

>>It will show you how many coins you got.


![][CheckRecord]

>>No matter you interrupt your concentration or not, you can check your planting record.


![][CRUD Tag]

>>CRUD Tag.


![][FuzzySearchFriend]

>>Use fuzzy search to get other users.(Search user name, eg: friend name: Qianwen, you can input Qia)

![][Logout]

>> Logout.




[Home]: ./img/Home.png
[Login]: ./img/Login.png
[SignInWithFaceBook]: ./img/SignWithFB
[SetTimer]: ./img/SetTimerForFocusing
[TimerGoes]: ./img/TimerGoes
[QuitFocusing]: ./img/QuitFocusing
[IfYouQuit]: ./img/ShowCoins
[CheckRecord]: ./img/CheckPlantingRecord
[CRUD Tag]: ./img/CRUDTag
[FuzzySearchFriend]: ./img/FuzzySearchFriend
[Logout]: ./img/Logout


Links:
gitlab-vue: https://gitlab.com/qianwenzhang/vueprojecttm/pipelines
gitlab-api: https://gitlab.com/qianwenzhang/web-api-projecttimemanagement

Heroku: https://taskmanagementqwzhang.herokuapp.com/