const apiURL = 'http://localhost:8080/tag'
describe('Tag PAGE', () => {
  beforeEach(() => {
    cy.visit(apiURL)
    // .it("body")
    // .then(tags => {
    //   tags.forEach(element => {
    //     cy.request("DELETE", `${apiURL}/deleteTag/${element._id}`)
    //   })
  })

  describe('shows a table in the page delete a tag', () => {
    it('Shows a table in the page', () => {
      cy.get('tbody')
        .find('tr')
        .should('have.length', 6)
      cy.get('tbody')
        .find('tr')
        .eq(2)
        .find('td')
        .eq(3)
        .find('a')
        .click()
      cy.get('button')
        .contains('Cancel')
        .click()
    })
    it('should jump to the create page', () => {
      cy.get('#addtag')
        .click()
    })
  })
})
