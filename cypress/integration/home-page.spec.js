describe('Home page', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080/')
  })

  it('Shows a header', () => {
    cy.get('.vue-title').should('contain', 'Put your phone down !!')
  })

  describe('Navigation bar', () => {
    it('Shows the required links', () => {
      cy.get('.navbar-nav')
        .eq(0)
        .within(() => {
          cy.get('.nav-item')
            .eq(0)
            .should('contain', 'Home')
          cy.get('.nav-item')
            .eq(1)
            .should('contain', 'Timer')
          cy.get('.nav-item')
            .eq(2)
            .should('contain', 'Tag')
          cy.get('.nav-item')
            .eq(3)
            .should('contain', 'Record')
          cy.get('.nav-item')
            .eq(4)
            .should('contain', 'Store')
        })
      cy.get('.navbar-nav')
        .eq(1)
        .within(() => {
          cy.get('.nav-item')
            .eq(0)
            .should('contain', 'About Us')
          cy.get('.nav-item')
            .eq(1)
            .should('contain', 'Contact Us')
        })
    })

    it('Redirects when links are clicked', () => {
      cy.get('[data-test=btn]').click()
      cy.url().should('include', '/#')
      cy.get('.navbar-nav')
        .eq(0)
        .find('.nav-item')
        .eq(0)
        .click()
      cy.url().should('include', '/#')
      // etc
    })

    it('Redirects when links are clicked', () => {
      cy.get('[data-test=timerbtn]').click()
      cy.url().should('include', '/timer')
      cy.get('.navbar-nav')
        .eq(0)
        .find('.nav-item')
        .eq(1)
        .click()
      cy.url().should('include', '/timer')
      // etc
    })

    it('Redirects when links are clicked', () => {
      cy.get('[data-test=tagbtn]').click()
      cy.url().should('include', '/tag')
      cy.get('.navbar-nav')
        .eq(0)
        .find('.nav-item')
        .eq(2)
        .click()
      cy.url().should('include', '/tag')
      // etc
    })
    it('Redirects when links are clicked', () => {
      cy.get('[data-test=recordbtn]').click()
      cy.url().should('include', '/record')
      cy.get('.navbar-nav')
        .eq(0)
        .find('.nav-item')
        .eq(3)
        .click()
      cy.url().should('include', '/record')
      // etc
    })
    it('Redirects when links are clicked', () => {
      cy.get('[data-test=storebtn]').click()
      cy.url().should('include', '/store')
      cy.get('.navbar-nav')
        .eq(0)
        .find('.nav-item')
        .eq(4)
        .click()
      cy.url().should('include', '/store')
      // etc
    })
    it('Redirects when links are clicked', () => {
      cy.get('[data-test=friendsbtn]').click()
      cy.url().should('include', '/friends')
      cy.get('.navbar-nav')
        .eq(0)
        .find('.nav-item')
        .eq(5)
        .click()
      cy.url().should('include', '/friends')
      // etc
    })
  })
})
