const apiURL = 'http://localhost:8080/record'

describe('Record PAGE', () => {
  beforeEach(() => {
    cy.visit(apiURL)
    // .it("body")
    // .then(tags => {
    //   tags.forEach(element => {
    //     cy.request("DELETE", `${apiURL}/deleteTag/${element._id}`)
    //   })
  })

  describe('shows planting records', () => {
    it('Shows a table in the page', () => {
      cy.wait(3000)
      cy.get('tbody')
        .find('tr')
        .eq(0)
        .find('td')
        .eq(8)
        .find('a')
        .click()
      cy.get('button')
        .contains('Cancel')
        .click()
    })
  })
})
