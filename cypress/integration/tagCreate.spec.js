const apiURL = 'http://localhost:8080/createtag'
describe('Tag PAGE', () => {
  beforeEach(() => {
    cy.visit(apiURL)
  })

  describe('shows the form of tag', () => {
    it('input some invalid thing', () => {
      cy.get('input')
        .eq(0)
        .type('Hello, World')
      cy.get('input')
        .eq(1)
        .type('Pink')
      cy.get('button')
        .eq(1)
        .click()
      cy.get('.typo__p')
        .should('contain.html', 'Successful!')

    })
  })
})
