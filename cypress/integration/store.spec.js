
describe('Store PAGE', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080/store')
  })
  describe('when load the page', () => {
    it('Shows a table in the page', () => {
      // cy.get('.vue-title').should('contain', 'Trees List')
      cy.get('tbody')
        .find("tr")
        .should("have.length",7)
    })
  })
})
