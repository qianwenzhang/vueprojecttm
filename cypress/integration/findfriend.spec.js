const apiURL = 'http://localhost:8080/friends'
describe('Tag PAGE', () => {
  beforeEach(() => {
    cy.visit(apiURL)
  })

  describe('input your friend name, get fuzzy search', () => {
    it('Should display the related users', () => {
      cy.get('input').type('ZQ')
      cy.get('button')
        .eq(1)
        .contains('search')
        .click()
      cy.get('tbody')
        .find('tr')
        .should('have.length', 1)
    })
  })

})
