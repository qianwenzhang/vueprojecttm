const apiURL = 'http://localhost:8080/timer'
describe('Tag PAGE', () => {
  beforeEach(() => {
    cy.visit(apiURL)
  })

  describe('shows the form info about timer', () => {
    it('Should display the trees you can plant', () => {
      cy.get('input').type('1')
      cy.get('select')
        .eq(0)
        .select('Cherry Bolossom')
      cy.get('textarea').type('This is a test')
      cy.get('button')
        .eq(1)
        .contains('start')
        .click()
    })
  })
})
