import Api from '@/services/api'

export default {
  fetchTrees () {
    return Api().get('/plantList')
  },
  fetchMyTrees (usertoken) {
    return Api().get(`/myplant/${usertoken}`)
  },
  fetchTreeDetail (name) {
    return Api().get(`/getOneTree/${name}`)
  }
}
